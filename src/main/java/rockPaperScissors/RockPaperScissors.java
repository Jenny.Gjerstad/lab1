package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> continueChoices = Arrays.asList("y", "n");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        while(true){
            System.out.printf("Let's play round %d%n", roundCounter);

            String humanChoice = playerChoice();
            String computerChoice = randomChoice();
            String choiceString = String.format("Human chose %s, computer chose %s.", humanChoice, computerChoice);

            //Check who won
            if (isWinner(humanChoice, computerChoice)) {
                System.out.printf(choiceString + " Human wins!");
                humanScore ++;
            }
            else if (isWinner(computerChoice, humanChoice)) {
                System.out.printf(choiceString + " Computer wins!");
                computerScore ++;
            }
            else {
                System.out.printf(choiceString + " It's a tie!");
            }
            System.out.printf("%nScore: human %s, computer %s.%n", humanScore, computerScore);
            
            //Check if human wants to play more
            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("y")) {
                roundCounter ++;
            }
            else {
                break;
            }
        }

        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    /**
     * 
     * @param choice1
     * @param choice2
     * @return true if choice1 beats choice2, false if not
     */
    public boolean isWinner(String choice1, String choice2) {
        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else if (choice1.equals("rock")) {
            return choice2.equals("scissors");
        }
        else {
            return choice2.equals("paper");
        }
    }
    /**
     * 
     * @param input
     * @param validInput
     * @return true if input is in the given list, false if not
     */
    public boolean validateInput(String input, List<String> validInput) {
        boolean valid = validInput.contains(input.toLowerCase());
        return valid;
    }

    /**
     * 
     * @return answer "y" or "n" from the user
     */
    public String continuePlaying() {
        while(true) {
            String answer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if(validateInput(answer, continueChoices)) {
                return answer;
            }
            else {
                System.out.printf("I do not understand %s. Could you try again?", answer);
            }
        }    
    }

    /**
     * 
     * @return the choice of the player in form of a string
     */
    public String playerChoice() {
        while(true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateInput(humanChoice, rpsChoices)) {
                return humanChoice;
            }
            else {
                System.out.printf("I do not understand %s. Could you try again?", humanChoice);
            }
        }
    }

    /**
     * 
     * @return a random choice between rock, paper and scissors in form of a string
     */
    public String randomChoice() {
        return rpsChoices.get(new Random(3).nextInt(rpsChoices.size()));
    }
}
